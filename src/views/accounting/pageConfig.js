/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-03-11 15:32:55
 * @LastEditors: youhui
 * @LastEditTime: 2022-03-12 14:10:38
 */
export const myHeaderList = [
  {
    hasSubmenu: false,
    index: '1',
    disabled: false,
    menuItemTitle: '概览1', // 不包含下级目录title
    submenuTitle: '', // 包含下级目录title
    submenu: []
  },
  {
    hasSubmenu: true,
    index: '2',
    disabled: false,
    menuItemTitle: '',
    submenuTitle: '概览2',
    submenu: [
      {
        hasSubmenu: false,
        index: '2-1',
        menuItemTitle: '概览2-1',
        submenuTitle: '',
        submenu: []
      },
      {
        hasSubmenu: true,
        index: '2-2',
        disabled: false,
        menuItemTitle: '',
        submenuTitle: '概览2-2',
        submenu: [
          {
            hasSubmenu: false,
            index: '2-2-1',
            disabled: false,
            menuItemTitle: '概览2-2-1',
            submenuTitle: '',
            submenu: []
          },
          {
            hasSubmenu: false,
            index: '2-2-2',
            disabled: true,
            menuItemTitle: '概览2-2-2',
            submenuTitle: '',
            submenu: []
          }
        ]
      }
    ]
  },
  {
    hasSubmenu: false,
    index: '3',
    disabled: true,
    menuItemTitle: '概览3',
    submenuTitle: '',
    submenu: []
  }
]
