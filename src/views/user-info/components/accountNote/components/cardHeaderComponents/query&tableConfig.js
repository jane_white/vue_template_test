/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-04-18 20:56:35
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-28 20:33:17
 */
// 卡片查询组件配置
export const cardDataSelecterQueryList = [
  {
    'component': 'SearchSelect',
    'componentType': 'FORM_SELECT_INPUT',
    'label': '交易类型',
    dropDownConfig: {
      multiple: false,
      selectOptions: [
        {
          id: 'outlay',
          label: '支出'
        },
        {
          id: 'income',
          label: '收入'
        }
      ]
    },
    'searchKey': 'tradeType'
  },
  {
    'component': 'SearchSelect',
    'componentType': 'FORM_SELECT_INPUT',
    'label': '账单类型',
    dropDownConfig: {
      multiple: true,
      selectOptions: [
        {
          id: 'salary',
          label: '工资'
        },
        {
          id: 'food',
          label: '餐饮'
        },
        {
          id: 'shopping',
          label: '购物'
        },
        {
          id: 'reimbursement',
          label: '报销'
        },
        {
          id: 'sophistication',
          label: '人情世故'
        },
        {
          id: 'utilityBill',
          label: '水电费'
        },
        {
          id: 'schoolFee',
          label: '学费'
        }
      ]
    },
    'searchKey': 'noteType'
  },
  {
    'component': 'SearchKeyword',
    'componentType': 'FORM_KEYWORD_INPUT',
    'label': '交易地点',
    dropDownConfig: {
      multiple: true,
      selectOptions: []
    },
    'searchKey': 'occurPlace'
  },
  {
    'component': 'SearchDate',
    'componentType': 'FORM_DATEPICK_INPUT',
    'label': '交易时间',
    'searchKey': 'occurTime'
  },
  {
    'component': 'SearchKeyword',
    'componentType': 'FORM_KEYWORD_INPUT',
    'label': '备注信息',
    dropDownConfig: {
      multiple: true,
      selectOptions: []
    },
    'searchKey': 'noteRemark'
  },
  {
    'component': 'SearchDate',
    'componentType': 'FORM_DATEPICK_INPUT',
    'label': '测试时间1',
    'searchKey': 'occurTime111'
  },
  {
    'component': 'SearchDate',
    'componentType': 'FORM_DATEPICK_INPUT',
    'label': '测试时间2',
    'searchKey': 'occurTime222'
  },
  {
    'component': 'SearchDate',
    'componentType': 'FORM_DATEPICK_INPUT',
    'label': '测试时间3',
    'searchKey': 'occurTime333'
  }
]

// 商品查询主页面主表表格配置
export const mainGoodsList = {
  checkAble: true,
  headerConfig: [
    {
      'prop': 'tradeType',
      'label': '交易类型',
      'type': 'FORM_TEXT_INPUT',
      // 'width': 160,
      style: [{ 'color': 'red' }],
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      'prop': 'noteType',
      'label': '账单类型',
      'type': 'FORM_TEXT_INPUT',
      // 'width': 160,
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      'prop': 'occurPlace',
      'label': '交易地点',
      'type': 'FORM_TEXT_INPUT',
      'sortable': false,
      // 'width': 160,
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      'prop': 'occurTime',
      'label': '交易时间',
      'type': 'FORM_TEXT_INPUT',
      'sortable': false,
      // 'width': 160,
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      'prop': 'tradeMoney',
      'label': '交易金额',
      'type': 'FORM_TEXT_INPUT',
      'sortable': false,
      // 'width': 160,
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      'prop': 'noteRemark',
      'label': '备注',
      'type': 'FORM_TEXT_INPUT',
      // 'width': 160,
      'align': 'center',
      'required': false,
      'editable': false
    },
    {
      label: '操作',
      type: 'rowOption',
      slotName: 'rowOption',
      // width: 260,
      align: 'center',
      fixed: 'right'
    }
  ],
  validRules: {},
  pageConfig: {
    'currentPage': 1,
    'pageSize': 10,
    'total': 0,
    'pageSizes': [10, 20, 50, 100]
  }
}
