/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-03-29 20:48:14
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-22 11:51:36
 */
export const cardInfoList = [
  [{
    tradeType: {
      label: '交易类型',
      value: { code: 'income', name: '收入' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'salary', name: '工资', color: '#41F1E5' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '上海xx公司' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-05 12:00:00' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '25000' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '如果真是这么多就好了。。' // 备注
    }
  },
  {
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'food', name: '餐饮', color: '#FF7E40' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '重庆小面' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-29 18:25:36' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '22' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '有点贵' // 备注
    }
  },
  {
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'food', name: '餐饮', color: '#FF7E40' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '重庆小面2' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-05 21:25:36' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '100' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '太贵了' // 备注
    }
  },
  {
    tradeType: {
      label: '交易类型',
      value: { code: 'income', name: '收入' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'food', name: '餐饮', color: '#FF7E40' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '黄焖鸡' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-29 20:25:36' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '30' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '退款了' // 备注
    }
  }],
  [{
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'shopping', name: '购物', color: '#FF4F00' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '万达广场' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-18 21:25:16' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '780' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '要剁手了！' // 备注
    }
  }],
  [{
    tradeType: {
      label: '交易类型',
      value: { code: 'income', name: '收入' } // 交易类别 收入income/支出outlay
    },

    noteType: {
      label: '账单类型',
      value: { code: 'reimbursement', name: '报销', color: '#007046' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '上海xx公司' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-31 12:00:00' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '3000' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '哎' // 备注
    }
  },
  {
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },
    noteType: {
      label: '账单类型',
      value: { code: 'sophistication', name: '人情世故', color: '#DB0058' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '不便透露' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-20 19:55:56' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '10000' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '悄悄的！' // 备注
    }
  }],
  [{
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },
    noteType: {
      label: '账单类型',
      value: { code: 'utilityBill', name: '水电费', color: '#FFCD40' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: '三单元1202' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-03-23 22:21:56' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '50' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '还行啊' // 备注
    }
  }],
  [{
    tradeType: {
      label: '交易类型',
      value: { code: 'outlay', name: '支出' } // 交易类别 收入income/支出outlay
    },
    noteType: {
      label: '账单类型',
      value: { code: 'schoolFee', name: '学费', color: '#E6FFE6' } // 账单类别--后续改为code
    },
    occurPlace: {
      label: '交易地点',
      value: 'xx小学' // 交易地点
    },
    occurTime: {
      label: '交易时间',
      value: '2022-02-03 08:32:18' // 交易时间
    },
    tradeMoney: {
      label: '交易金额',
      value: '3600' // 交易金额
    },
    noteRemark: {
      label: '备注',
      value: '这就是投资啊' // 备注
    }
  }]
]
