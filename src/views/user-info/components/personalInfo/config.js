/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-03-27 11:28:49
 * @LastEditors: youhui
 * @LastEditTime: 2022-03-29 17:12:42
 */
export const selfInfo = {
  nickName: '简白',
  codeAge: '2年6个月',
  selfBasicInfo: [
    {
      key: 1,
      label: '用户昵称',
      value: '简白'
    },
    {
      key: 2,
      label: '用户ID',
      value: '0001'
    },
    {
      key: 3,
      label: '性别',
      value: '男'
    },
    {
      key: 4,
      label: '个人简介',
      value: '这是一条简单的简介信息'
    },
    {
      key: 5,
      label: '所在地区',
      value: '北京市-朝阳区'
    },
    {
      key: 6,
      label: '出生日期',
      value: '1998-05'
    },
    {
      key: 7,
      label: '开始工作',
      value: '2021-07'
    }
  ],
  selfEducationInfo: [
    {
      label: '学校名称',
      value: ''
    },
    {
      label: '专业',
      value: ''
    },
    {
      label: '入学时间',
      value: ''
    },
    {
      label: '学历',
      value: ''
    }
  ]
}
