/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-04-18 20:30:13
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-18 20:30:18
 */
const SearchComponentMixin = {
  model: {
    prop: 'searchValue',
    event: 'change'
  },
  props: {
    label: {
      type: String,
      required: true
    },
    searchKey: {
      type: String,
      required: true
    },
    searchValue: {
      required: true
    },
    readOnly: {
      type: Boolean,
      default: false
    },
    extraConfig: {
      type: Object,
      default: () => {
        return {}
      }
    }
  },
  computed: {
    computedValue: {
      get: function() {
        return this.searchValue
      },
      set: function(value) {
        this.$emit('change', value)
      }
    }
  },
  watch: {
    searchValue: {
      handler(newValue, oldValue) {
        // console.log('get', newValue)
      },
      deep: true
    }
  }
}

export default SearchComponentMixin
