/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-04-17 15:23:44
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-25 17:11:32
 */
export function colorHex(color) {
  var strHex = '#'
  // 把RGB的3个数值变成数组
  var colorArr = color.replace(/(?:\(|\)|rgb|RGB)*/g, '').split(',')
  // 转成16进制
  for (var i = 0; i < colorArr.length; i++) {
    var hex = Number(colorArr[i]).toString(16)
    if (hex === '0') {
      hex += hex
    }
    hex = hex.length === 1 ? '0' + hex : hex
    strHex += hex
  }
  return strHex
}

export function changeType(val) {
  const date = new Date(Date.parse(val))
  const Y = date.getFullYear()
  const M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
  const D = date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate()
  //  + ' '
  // let h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours()) + ':'
  // let m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes())
  // let s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds())
  // val = Y + '-' + M + '-' + D + h + m
  val = Y + '-' + M + '-' + D
  return val
}
