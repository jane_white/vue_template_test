/*
 * @Description:
 * @Autor: youhui
 * @Date: 2021-08-20 09:42:45
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-06 20:55:02
 */
module.exports = {

  title: '练习项目',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false
}
