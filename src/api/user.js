/*
 * @Description:
 * @Autor: youhui
 * @Date: 2021-08-20 09:42:45
 * @LastEditors: youhui
 * @LastEditTime: 2022-06-16 20:34:07
 */
import request from '@/utils/request'
import { baseUrl } from '@/api/index'

export function login(data) {
  return request({
    // url: '/vue-admin-template/user/login',
    url: baseUrl + '/income/expenditure/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    // url: '/vue-admin-template/user/logout',
    url: baseUrl + '/income/expenditure/user/logout',
    method: 'post'
  })
}
