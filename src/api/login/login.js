/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-03-25 11:58:53
 * @LastEditors: youhui
 * @LastEditTime: 2022-06-18 11:09:53
 */
import { baseUrl } from '@/api/index'
export default {
  // 注册
  systemRegister: {
    url: baseUrl + '/income/expenditure/user/registered',
    method: 'POST',
    disableSuccessMsg: false
  },

  // 登录--此处作废 登录走项目原有逻辑
  systemLogin: {
    url: baseUrl + '/income/expenditure/user/login',
    method: 'POST',
    disableSuccessMsg: false
  }
}
