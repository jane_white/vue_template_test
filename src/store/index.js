/*
 * @Description:
 * @Autor: youhui
 * @Date: 2021-08-20 09:42:45
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-06 21:06:25
 */
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

import globalStyle from './modules/globalStyleSettings'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    globalStyle
  },
  getters
})

export default store
