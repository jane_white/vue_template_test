/*
 * @Description: 自定义全局样式
 * @Autor: youhui
 * @Date: 2022-04-06 21:01:03
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-26 15:53:50
 */

const state = {
  background: '',
  dialogOpacity: 1
}

const mutations = {
  saveSelfThemeParams: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  }
}

const actions = {
  saveSelfThemeParams({ commit }, data) {
    commit('saveSelfThemeParams', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
