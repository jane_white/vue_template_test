/*
 * @Description:
 * @Autor: youhui
 * @Date: 2021-08-20 09:42:45
 * @LastEditors: youhui
 * @LastEditTime: 2022-06-15 11:24:15
 */

import Vue from 'vue'
import {
  DropdownMenu,
  DropdownItem,
  Row,
  Col,
  Button,
  Dialog,
  Field,
  Popover,
  Grid,
  GridItem,
  Card,
  Collapse,
  CollapseItem,
  Tabs,
  Tab,
  List,
  Cell,
  Divider
} from 'vant'
Vue.use(Row).use(Col)
Vue.use(DropdownMenu)
Vue.use(Button)
Vue.use(DropdownItem)
Vue.use(Dialog)
Vue.use(Field)
Vue.use(Popover)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(Card)
Vue.use(Collapse)
Vue.use(CollapseItem)
Vue.use(Tabs)
Vue.use(Tab)
Vue.use(List)
Vue.use(Cell)
Vue.use(Divider)

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import request from '@/utils/request'
Vue.prototype.$request = request

// 引入video
import VideoPlayer from 'vue-video-player'
Vue.use(VideoPlayer)

// 引入弹幕
import { vueBaberrage } from 'vue-baberrage'
Vue.use(vueBaberrage)

// 引入好看的背景
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

// import XDcloudUi from 'x-dcloud-ui' // 引入组件库

// Vue.use(XDcloudUi)

// import {
//   XSvgIcon,
//   XModal,
//   XPersonTag,
//   XPersonPopover,
//   XPersonModal,
//   XEllipsis,
//   XMoneyInput,
//   XNumberInput,
//   XRichText,
//   XTagGroup,
//   XDepartmentModal,
//   XDepartmentTag,
//   XRoleModal,
//   XResizeable,
//   XPersonAvatar,
//   XDrawer,
//   XLoading,
//   XEmptyPage,
//   XDate,
//   XBlockTable,
//   XVxeTable,
//   XSelect,
//   XEditor
// } from 'x-dcloud-ui'

// import { XDate, XModal } from 'x-dcloud-ui'
// Vue.use(XDate)
// Vue.use(XModal)

// Vue.use(XSvgIcon)
// Vue.use(XBlockTable)
// Vue.use(XModal)
// Vue.use(XDrawer)
// Vue.use(XEllipsis)
// Vue.use(XMoneyInput)
// Vue.use(XNumberInput)
// Vue.use(XRichText)
// Vue.use(XResizeable)
// Vue.use(XPersonTag)
// Vue.use(XPersonAvatar)
// Vue.use(XPersonModal)
// Vue.use(XDepartmentModal)
// Vue.use(XPersonPopover)
// Vue.use(XLoading)
// Vue.use(XEmptyPage)
// Vue.use(XDate)
// Vue.use(XTagGroup)
// Vue.use(XSelect)
// Vue.use(XRoleModal)
// Vue.use(XVxeTable)
// Vue.use(XEditor)
// Vue.use(XDepartmentTag)
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

// 引入鼠标右键插件
import VueContextMenu from 'vue-contextmenu'
Vue.use(VueContextMenu)

// 引入更美观的消息提示
import Toast from 'vue-toastification'

import 'vue-toastification/dist/index.css'

const options = {
  transition: 'Vue-Toastification__bounce',
  maxToasts: 3,
  newestOnTop: true,
  position: 'top-right', // 出现位置
  timeout: 2000, // 消失时间
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: false,
  draggable: true,
  draggablePercent: 0.7,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: 'button',
  icon: true,
  rtl: false
}

Vue.use(Toast, options)

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

Vue.config.productionTip = false

Vue.prototype.$bus = new Vue()

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
