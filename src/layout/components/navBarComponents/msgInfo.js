/*
 * @Description:
 * @Autor: youhui
 * @Date: 2022-04-25 10:07:20
 * @LastEditors: youhui
 * @LastEditTime: 2022-04-26 14:30:46
 */
export const msgInfoList = [
  {
    msgId: '1',
    msgContent: '这是消息11',
    msgTime: '2022-04-26 10:10:20',
    msgSender: {
      id: '001',
      name: '佑辉'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  },
  {
    msgId: '2',
    msgContent: '最新版本即将发布！',
    msgTime: '2022-04-24 23:10:20',
    msgSender: {
      id: '001',
      name: '应用'
    },
    msgType: 'appNotify',
    status: 'yetread'
  },
  {
    msgId: '3',
    msgContent: '这是消息孙菲菲',
    msgTime: '2022-04-25 21:10:20',
    msgSender: {
      id: '001',
      name: '菲菲'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  },
  {
    msgId: '4',
    msgContent: '请及时修改初始密码！',
    msgTime: '2022-04-23 13:16:20',
    msgSender: {
      id: '001',
      name: '系统'
    },
    msgType: 'appNotify',
    status: 'hasread'
  },
  {
    msgId: '5',
    msgContent: '这是消息55',
    msgTime: '2022-04-20 11:10:20',
    msgSender: {
      id: '001',
      name: '佑辉'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  },
  {
    msgId: '1',
    msgContent: '这是消息11',
    msgTime: '2022-04-20 10:10:20',
    msgSender: {
      id: '001',
      name: '佑辉'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  },
  {
    msgId: '2',
    msgContent: '这是消息22',
    msgTime: '2022-04-14 23:10:20',
    msgSender: {
      id: '001',
      name: '佑辉'
    },
    msgType: 'appNotify',
    status: 'yetread'
  },
  {
    msgId: '3',
    msgContent: '这是消息33',
    msgTime: '2022-04-15 11:10:20',
    msgSender: {
      id: '001',
      name: '系统'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  },
  {
    msgId: '4',
    msgContent: '这是消息44',
    msgTime: '2022-04-13 13:16:20',
    msgSender: {
      id: '001',
      name: '应用'
    },
    msgType: 'appNotify',
    status: 'hasread'
  },
  {
    msgId: '5',
    msgContent: '这是消息55',
    msgTime: '2022-04-10 11:10:20',
    msgSender: {
      id: '001',
      name: '系统'
    },
    msgType: 'sysNotify',
    status: 'yetread'
  }
]
